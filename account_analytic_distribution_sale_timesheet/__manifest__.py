{
    "name": "Account Analytic Distribution with Sale Timesheet",
    "summary": """Base Module for Dynamic Analytic Distribution""",
    "version": "1.0.1.0.0",
    "category": "Accounting & Finance",
    "website": "https://gitlab.com/flectra-community/account-analytic",
    "author": "Flectra Community",
    "license": "AGPL-3",
    "depends": [
        "account_analytic_distribution",
        "sale_timesheet",
    ],
    "data": [
        'views/product_views.xml',
    ],
    "installable": True,
}
