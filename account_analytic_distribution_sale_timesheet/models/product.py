from flectra import api, fields, models


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    analytic_distribution_id = fields.Many2one(
            comodel_name='account.analytic.distribution',
            string='Analytic distribution',
    )
