{
    "name": "Account Analytic Distribution Dynamic",
    "summary": """Base Module for Dynamic Analytic Distribution""",
    "version": "1.0.1.0.0",
    "category": "Accounting & Finance",
    "website": "https://gitlab.com/flectra-community/account-analytic",
    "author": "Flectra Community",
    "license": "AGPL-3",
    "depends": [
        "account_analytic_distribution",
    ],
    "data": [
        'views/account_analytic_distribution_view.xml',
    ],
    "installable": True,
}
