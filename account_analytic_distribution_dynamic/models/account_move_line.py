from flectra import api, fields, models


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    def _analytic_line_distributed_prepare(self, rule):

        if rule.type != 'existing':
            return super()._analytic_line_distributed_prepare(rule)

        res = self._prepare_analytic_line()
        if not res:
            return False

        res = res[0]

        amount = (res.get('amount') * rule.percent) / 100.0
        res['amount'] = amount

        return res
