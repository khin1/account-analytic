# Copyright 2017 - Tecnativa - Vicent Cubells
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from flectra.tests import common
from flectra.exceptions import ValidationError


class TestAnalyticDistribution(common.SavepointCase):
    @classmethod
    def setUpClass(self):
        super(TestAnalyticDistribution, self).setUpClass()
        self.account1 = self.env['account.analytic.account'].create({
            'name': 'Test account #1',
        })
        self.account2 = self.env['account.analytic.account'].create({
            'name': 'Test account #2',
        })
        self.account3 = self.env['account.analytic.account'].create({
            'name': 'Test account #3',
        })
        self.account4 = self.env['account.analytic.account'].create({
            'name': 'Test account #4',
        })
        self.account5 = self.env['account.analytic.account'].create({
            'name': 'Test account #5',
        })

        self.product1 = self.env['product.product'].create({
            'name': 'test product 01',
            'income_analytic_account_id': self.account2.id,
            'expense_analytic_account_id': self.account2.id}
        )
        self.product2 = self.env['product.product'].create({
            'name': 'test product 01',
            'income_analytic_account_id': self.account3.id,
            'expense_analytic_account_id': self.account5.id}
        )
        self.product3 = self.env['product.product'].create({
            'name': 'test product 01',
            'income_analytic_account_id': self.account4.id,
            'expense_analytic_account_id': self.account5.id}
        )

        self.invoice_model = self.env['account.invoice']
        self.distribution = self.env['account.analytic.distribution'].create({
            'name': 'Test distribution initial',
            'rule_ids': [
                (0, 0, {
                    'sequence': 10,
                    'percent': 75.00,
                    'analytic_account_id': self.account1.id,
                }),
                (0, 0, {
                    'sequence': 20,
                    'percent': 25.00,
                    'type': 'product',
                }),
            ]
        })

        self.distribution2 = self.env['account.analytic.distribution'].create({
            'name': 'Test distribution product',
            'rule_ids': [
                (0, 0, {
                    'sequence': 10,
                    'percent': 100.00,
                    'type': 'product',
                }),
                (0, 0, {
                    'sequence': 20,
                    'percent': 80.00,
                    'type': 'product',
                }),
            ]
        })

        self.user_type = self.env.ref('account.data_account_type_revenue')
        self.invoice = self.invoice_model.create({
            'partner_id': self.env.ref('base.res_partner_12').id,
            'invoice_line_ids': [(0, 0, {
                'name': 'Product Test',
                'quantity': 1.0,
                'uom_id': self.env.ref('product.product_uom_unit').id,
                'price_unit': 100.0,
                'product_id': self.product1.id,
                'account_id': self.env['account.account'].search([
                    ('user_type_id', '=', self.user_type.id)], limit=1).id,
            })]
        })

    def test_product_distribution(self):
        invoice = self.invoice_model.create({
            'partner_id': self.env.ref('base.res_partner_12').id,
            'invoice_line_ids': [(0, 0, {
                'name': 'Product Test',
                'quantity': 4.0,
                'uom_id': self.env.ref('product.product_uom_unit').id,
                'price_unit': 100.0,
                'product_id': self.product2.id,
                'analytic_distribution_id': self.distribution2.id,
                'account_id': self.env['account.account'].search([
                    ('user_type_id', '=', self.user_type.id)], limit=1).id,
            })]
        })
        invoice.action_invoice_open()
        self.assertEqual(len(self.account3.line_ids.ids), 2)
        self.assertEqual(sum(self.account3.line_ids.mapped('amount')), 720)
        self.assertEqual(len(self.account5.line_ids.ids), 0)

    def test_partner_invoice(self):
        # Save values to compare later
        count1 = len(self.account1.line_ids.ids)
        count2 = len(self.account2.line_ids.ids)
        amount1 = sum(self.account1.line_ids.mapped('amount'))
        amount2 = sum(self.account2.line_ids.mapped('amount'))
        self.invoice.invoice_line_ids[0].analytic_distribution_id = self.distribution.id
        self.invoice.journal_id.group_invoice_lines = True
        self.invoice.action_invoice_open()
        # One line by account has been created only
        self.assertEqual(len(self.account1.line_ids.ids), count1 + 1)
        self.assertEqual(len(self.account2.line_ids.ids), count2 + 1)
        # Check amount
        self.assertAlmostEqual(
                self.account1.balance, amount1 + 75.0)
        self.assertAlmostEqual(
                self.account2.balance, amount2 + 25.0)
