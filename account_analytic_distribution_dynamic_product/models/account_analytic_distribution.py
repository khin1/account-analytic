from flectra import _, api, fields, models


class AccountAnalyticDistributionRule(models.Model):
    _inherit = "account.analytic.distribution.rule"

    type = fields.Selection(
            selection_add=[
                ('product', 'Product'),
            ],
    )
